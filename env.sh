export IOCNAME="OCTOPUS-LLRF1:Ctrl-IOC-001"
export AS_TOP=/opt/nonvolatile
export IOCDIR="OCTOPUS-LLRF1_Ctrl-IOC-001"
#this variable define which snippet will be loaded for this IOC
#export SIS8300LLRF_VER=dev_acq_time
#export SIS8300LLRF_VER=dbg_speed

export EPICS_CA_MAX_ARRAY_BYTES=16777300
export LLRF_PREFIX="OCTOPUS-010:RFS-LLRF-101"
export LLRF_DIGRTM_PREFIX="OCTOPUS-010"

export LLRF_DIG_R_1="RFS-DIG-101"
export LLRF_DIG_R_2="RFS-DIG-102"
export LLRF_DIG_R_3="RFS-DIG-103"
export LLRF_DIG_R_4="RFS-DIG-104"

export LLRF_RFM_R_1="RFS-RFM-101"
export LLRF_RFM_R_2="RFS-RFM-102"
export LLRF_RFM_R_3="RFS-RFM-103"
export LLRF_RFM_R_4="RFS-RFM-104"

#export F_SAMPLING=96.25
#export F_SYSTEM=352.210
export F_SAMPLING=117.403333333
export F_SYSTEM=352.210
export NEARIQN=14
export NEARIQM=3

export LLRF_SLOT_1=6
export LLRF_SLOT_2=8
export LLRF_SLOT_3=9
export LLRF_SLOT_4=11

# Move this to llrfsystem with different substitutions?
export CAVITYCH=0
export CAVFWDCH=2
export PWRFWDCH=6
export TABLE_SMNM_MAX=1000 # Double check this

# Timing
export EVRPREFIX="OCTOPUS-010:Ctrl-EVR-101"
export EVRSLOT="0e:00.0"
