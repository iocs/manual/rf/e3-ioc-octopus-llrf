require essioc

# Load LLRF
require sis8300llrf, 5.29.4+0
require llrfsystem, 3.19.1+2

epicsEnvSet("LLRF_P", "OCTOPUS-010")
epicsEnvSet("LLRF_R", "RFS-LLRF-101")
epicsEnvSet("LLRF_DIG_R_1", "RFS-DIG-101")
epicsEnvSet("LLRF_RFM_R_1", "RFS-RFM-101")
epicsEnvSet("LLRF_SLOT_1", "6")
epicsEnvSet("TSELPV", "OCTOPUS-010:RFS-EVR-101:EvtACnt-I.TIME")

epicsEnvSet("LPSEN", "0")
epicsEnvSet("RFSTARTCNT", "OCTOPUS-010:RFS-EVR-101:EvtACnt-I")
epicsEnvSet("RFSTID", "1")

## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh", "PWRFWDCH=6, PEVR=OCTOPUS-010:RFS-EVR-101:,SCL_TS2=")

ndsSetTraceLevel 2
dbl > PVs.list
## For commands to be run after iocInit, use the function afterInit()

# Call iocInit to start the IOC
iocInit()
date
